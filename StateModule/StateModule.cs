﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IridiumServer;

namespace Modules
{
    public class StateModule : Module
    {
        public StateModule()
        {
            Name = "StateModule";
            DisplayName = "State Module";

        }
        public override void RunModule()
        {
            base.RunModule();
            State = ModuleState.Inactive;
            RefreshGUI();
        }
    }
}
