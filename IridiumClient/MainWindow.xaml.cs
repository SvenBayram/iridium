﻿using IridiumInterfaces;
using IridiumServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace IridiumClient
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string baseAddress;
        private int clientID;
        private ChannelFactory<IWCFModuleService> channel;
        private IWCFModuleService proxy;
        private List<string> availableModules;
        private Timer dataTimer;

        Queue<ComplexData> ReceivedDataQueue = new Queue<ComplexData>();


        public MainWindow()
        {
            InitializeComponent();
#if DEBUG
            tbAddress.Text = @"http://192.168.0.143:4711/ModuleService/";
#endif

        }
        private void BtnGet_Click(object sender, RoutedEventArgs e)
        {
            clientID = proxy.RegisterClient();
            availableModules = proxy.ListAvailableModules();
            string output = "";
            foreach (string module in availableModules)
            {
                output += module + "\r\n";
            }
            tbOutput.Text = output;
            dataTimer = new Timer(ReceiveData, null, 0, 500);
        }

        private void BtnConnect_Click(object sender, RoutedEventArgs e)
        {
            baseAddress = tbAddress.Text;
            channel = new ChannelFactory<IWCFModuleService>(new WSDualHttpBinding(), new EndpointAddress(baseAddress));
            proxy = channel.CreateChannel();
            
        }

        private void ReceiveData(object state)
        {
            if (proxy.DataAvailable(clientID) > 0)
            {
                for (int i = 0; i < proxy.DataAvailable(clientID); i++)
                {
                    ReceivedDataQueue.Enqueue(proxy.ReceiveObject(clientID));
                }
            }
        }
    }
}
