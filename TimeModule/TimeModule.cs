﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IridiumServer;
using System.Threading;
using System.Net.Sockets;
using System.Collections;

namespace Modules
{
    public class TimeModule : Module
    {
        public TimeModule()
        {
            Name = "TimeModule";
            DisplayName = "Time Module";
        }

        public override void RunModule()
        {
            base.RunModule();
            State = ModuleState.Inactive;
            RefreshGUI();
        }
    }
}
