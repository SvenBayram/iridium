﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Forms;

namespace IridiumServer
{
    public class ModuleLoader
    {
        private List<Module> modules = new List<Module>();
        private string moduleDirectory;
        private string[] moduleFileNames;
        private int loadedModulesCount = 0;
        private int startedModulesCount = 0;

        public int LoadedModulesCount { get => loadedModulesCount; set => loadedModulesCount = value; }
        public int StartedModulesCount { get => startedModulesCount; set => startedModulesCount = value; }

        public ModuleLoader()
        {
            LoadModuleFiles();
            LoadModules();
        }

        public void LoadModuleFiles()
        {
            DirectoryInfo di = new DirectoryInfo(@"modules\");
            FileInfo[] Files = di.GetFiles();
            moduleFileNames = new string[Files.Length];
            int i = 0;
            foreach (FileInfo file in Files)
            {
                moduleDirectory = file.DirectoryName;
                moduleFileNames[i] = file.Name.Replace(".dll", "");
                i++;
            }
        }

        public void LoadModules()
        {
            foreach (string item in moduleFileNames)
            {
                Assembly assembly = Assembly.LoadFile(moduleDirectory + "\\" + item + ".dll");
                Type type = assembly.GetType("Modules." + item);
                Module module = (Module)Activator.CreateInstance(type);
                modules.Add(module);
                LoadedModulesCount++;
            }
        }

        public void StartModule(string startModule)
        {
            foreach (Module module in modules)
            {
                if (module.Name == startModule && module.Started == false)
                {
                    module.RunModule();
                }
            }
            UpdateStartedModuleCount();
        }

        private void UpdateStartedModuleCount()
        {
            StartedModulesCount = 0;

            foreach (Module module in modules)
            {
                if (module.Started)
                {
                    StartedModulesCount++;
                }
            }
        }

        public void StartAllModules()
        {
            foreach (Module module in modules)
            {
                if (module.Started == false)
                {
                    module.RunModule();
                }
            }
            UpdateStartedModuleCount();
        }

        public void StopModule(string stopModule)
        {
            foreach (Module module in modules)
            {
                if (module.Name == stopModule && module.Started == true)
                {
                    if (module.State == ModuleState.Inactive)
                    {
                        module.StopModule();
                    }
                    else
                    {
                        MessageBox.Show($"Can't unload {module.DisplayName}. The module is currently in the state {module.State}.", $"Can't unload {module.DisplayName}!");
                    }
                }
            }
            UpdateStartedModuleCount();
        }

        public List<Module> GetModules()
        {
            return modules;
        }

        public string[] GetNames()
        {
            return moduleFileNames;
        }

        public string[] GetDisplayNames()
        {
            string[] displayNames = new string[modules.Count];
            for (int i = 0; i < modules.Count; i++)
            {
                displayNames[i] = modules[i].DisplayName;
            }
            return displayNames;
        }

        public List<string> GetStartedModulNames()
        {
            List<string> startedModules = new List<string>();
            foreach (Module module in modules)
            {
                if (module.Started)
                {
                    startedModules.Add(module.Name);
                }
            }
            return startedModules;
        }
    }
}
