﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace IridiumServer
{
    class Client
    {
        private int clientID;
        private string clientIP;
        public Client(int clientID, string clientIP)
        {
            this.clientID = clientID;
            this.clientIP = clientIP;
        }

        public int ClientID { get => clientID; }
        public string ClientIP { get => clientIP; }
    }
}
