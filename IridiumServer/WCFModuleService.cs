﻿using IridiumServer;
using System;
using System.Collections.Generic;
using System.Linq; 
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Windows;
using IridiumInterfaces; 

namespace IridiumServer
{
    [ServiceBehavior(InstanceContextMode=InstanceContextMode.Single)]
    public class WCFModuleService : IWCFModuleService
    {
        private List<Module> moduleList;
        private List<string> moduleNameList;
        private List<Client> clientList;
        private List<ComplexData> dataForClientList;
        private List<ComplexData> dataForModuleList;
        private int nextClientID = 0;

        public int DataAvailable(int requestingClient)
        {
            int count = 0;
            foreach (ComplexData item in dataForClientList)
            {
                if (item.ClientAffiliation == requestingClient)
                {
                    count++;
                }
            }
            return count;
        }

        public ModuleState GetModuleState(string module)
        {
            foreach (Module item in moduleList)
            {
                if (item.Name == module)
                {
                    return item.State;
                }
            }

            MessageBox.Show($"Wrong name. No '{module}' found.","Wrong name.");

            return ModuleState.Error;
        }

        public List<string> ListAvailableModules()
        {
            moduleList = ((MainWindow)Application.Current.MainWindow).GetModuleList(); 
            moduleNameList = new List<string>();
            if (moduleList != null)
            {
                moduleNameList.Clear();
                foreach (Module item in moduleList)
                {
                    if (item.Started == true)
                    {
                        moduleNameList.Add(item.DisplayName);
                    }
                }
            }
            return moduleNameList;
        }

        public ComplexData ReceiveObject(int requestingClient)
        {
            if (dataForClientList.Count != 0)
            {
                foreach (ComplexData item in dataForModuleList)
                {
                    if (item.ClientAffiliation == requestingClient)
                    {
                        return item;
                    }
                }
                throw new Exception("No data available");
            }
            else
            {
                throw new Exception("No data available");
            }
        }

        public int RegisterClient()
        {
            nextClientID++;

            OperationContext context = OperationContext.Current;
            MessageProperties prop = context.IncomingMessageProperties;
            RemoteEndpointMessageProperty endpoint = prop[RemoteEndpointMessageProperty.Name] as RemoteEndpointMessageProperty;
            string ip = endpoint.Address;

            clientList.Add(new Client(nextClientID, ip));

            return nextClientID;
        }

        public void SendObject(ComplexData data)
        {
            dataForModuleList.Add(data);
        }
    }
}
