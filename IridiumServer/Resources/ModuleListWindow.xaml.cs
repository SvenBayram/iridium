﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

    
namespace IridiumServer
{
    /// <summary>
    /// Interaktionslogik für ModuleList.xaml
    /// </summary>
    public partial class ModuleListWindow : Window
    {
        private List<ListBoxItem> listBoxItemList = new List<ListBoxItem>();
        private List<CheckBox> checkBoxList = new List<CheckBox>();
        private List<string> selectedModules = new List<string>();
        private List<string> startedModule = new List<string>();
        private string[] displayNames;
        private string[] names;
        private ModuleLoader loader;

        public ModuleListWindow(ModuleLoader loader)
        {
            InitializeComponent();
            this.loader = loader;

            names = loader.GetNames();
            displayNames = loader.GetDisplayNames();
            startedModule = loader.GetStartedModulNames();

            for (int i = 0; i < names.Length; i++)
            {
                ListBoxItem item = new ListBoxItem()
                {
                    Focusable = false,
                    Style = (Style)Application.Current.Resources["ListBoxItemStyle"],
                };
                listBoxItemList.Add(item);

                StackPanel stack = new StackPanel()
                {
                    Height = 25,
                    Orientation = Orientation.Horizontal
                };
                CheckBox checkBox = new CheckBox()
                {
                    Name = names[i],
                    Style = (Style)Application.Current.Resources["CheckBoxStyle"],
                    Margin = new Thickness(0, 2, 0, 0)
                };
                checkBoxList.Add(checkBox);
                TextBlock textBlock = new TextBlock()
                {
                    Text = displayNames[i],
                    FontSize = 20,
                    Margin = new Thickness(10, -3, 0, 0),
                    Foreground = (Brush)Application.Current.Resources["StandartTextColor"]
                };
                stack.Children.Add(checkBox);
                stack.Children.Add(textBlock);

                item.Content = stack;
            }

            foreach (string name in names)
            {
                for (int i = 0; i < startedModule.Count(); i++)
                {
                    if (name == startedModule[i])
                    {
                        foreach (CheckBox checkbox in checkBoxList)
                        {
                            if (checkbox.Name == name)
                            {
                                checkbox.IsChecked = true;
                            }
                        }
                    }
                }
            }

            foreach (ListBoxItem item in listBoxItemList)
            {
                ListBoxModules.Items.Add(item);
            }
        }

        private void BtnLoad_Click(object sender, RoutedEventArgs e)
        {
            foreach (CheckBox checkBox in checkBoxList)
            {
                if (checkBox.IsChecked == true ? true : false)
                {
                    loader.StartModule(checkBox.Name);
                }
                else
                {
                    foreach (string startedModule in startedModule)
                    {
                        if (checkBox.Name == startedModule)
                        {
                            loader.StopModule(checkBox.Name);
                        }
                    }

                }
            }
            ((MainWindow)Application.Current.MainWindow).RefreshModuleList();
            Close();
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
