﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace IridiumServer.Resources
{
    /// <summary>
    /// Interaktionslogik für ModuleGUI.xaml
    /// </summary>
    public partial class ModuleGUI : UserControl
    {
        private string name;
        private int connectedClients;
        public ModuleState state;

        public ModuleGUI(string name, ModuleState state)
        {
            InitializeComponent();
            this.name = name;
            this.state = state;

            nameBlock.Text = this.name;
            clientCount.Text = connectedClients.ToString();

            connectedClients = 0;
            
            Refresh();
        }

        public void Refresh()
        {
            switch (state)
            {
                case ModuleState.Active:
                    statusBlock.Foreground = Brushes.Green;
                    break;

                case ModuleState.Inactive:
                    statusBlock.Foreground = Brushes.Yellow;
                    break;

                case ModuleState.Working:
                    statusBlock.Foreground = Brushes.Yellow;
                    break;

                case ModuleState.Error:
                    statusBlock.Foreground = Brushes.Red;
                    break;
            }


            if (connectedClients >= 5)
            {
                clientCount.Foreground = Brushes.Yellow;
            }
            else
            {
                clientCount.Foreground = Brushes.Green;
            }
            if (connectedClients >= 10)
            {
                clientCount.Foreground = Brushes.Red;
            }
            clientCount.Text = connectedClients.ToString();
            statusBlock.Text = state.ToString();
        }
    }
}
