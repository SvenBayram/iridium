﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace IridiumServer
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string baseAddress;
        private int loadedModules = 0;
        private int startedModules = 0;
        private DispatcherTimer refreshTimer = new DispatcherTimer();
        private List<Module> modules;
        private string[] moduleNames;
        private ModuleLoader loader;

        public string[] ModuleNames { get => moduleNames; set => moduleNames = value; }

        public MainWindow()
        {
            InitializeComponent();

            refreshTimer.Tick += RefreshGUI;
            refreshTimer.Interval = new TimeSpan(0, 0, 0, 0, 100);
            refreshTimer.Start();
            
            loader = new ModuleLoader();
            ModuleNames = loader.GetNames();

            InitializeModuleService();
        }

        private void InitializeModuleService()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    baseAddress = @"http://" + ip.ToString() + ":4711/ModuleService";
                }
            }

            Console.WriteLine(baseAddress);

            ServiceHost serviceHost = new ServiceHost(typeof(WCFModuleService), new Uri(baseAddress));
            serviceHost.Open();
            Console.WriteLine("ModuleServer is open");
            
        }

        public List<Module> GetModuleList()
        {
            return modules;
        }

        public void RefreshModuleList()
        {
            modules = loader.GetModules();
        }

        public void RefreshGUI(object sender, EventArgs e)
        {
            loadedModules = loader.LoadedModulesCount;
            startedModules = loader.StartedModulesCount;
            if (modules != null)
            {
                foreach (Module module in modules)
                {
                    if (module.Started)
                    {
                        module.RefreshGUI();
                    }
                }
            }

            TBLoadedModules.Text = loadedModules.ToString();
            TBStartedModules.Text = startedModules.ToString();
            ConnectedClients.Text = "Not Implemented yet";
        }

        public StackPanel GetPanel()
        {
            return ModulPanel;
        }

        private void BtnFile_Click(object sender, RoutedEventArgs e)
        {
            (sender as Button).ContextMenu.IsEnabled = true;
            (sender as Button).ContextMenu.PlacementTarget = (sender as Button);
            (sender as Button).ContextMenu.Placement = System.Windows.Controls.Primitives.PlacementMode.Bottom;
            (sender as Button).ContextMenu.IsOpen = true;
        }

        private void BtnTools_Click(object sender, RoutedEventArgs e)
        {
            (sender as Button).ContextMenu.IsEnabled = true;
            (sender as Button).ContextMenu.PlacementTarget = (sender as Button);
            (sender as Button).ContextMenu.Placement = System.Windows.Controls.Primitives.PlacementMode.Bottom;
            (sender as Button).ContextMenu.IsOpen = true;
        }

        private void BtnHelp_Click(object sender, RoutedEventArgs e)
        {
            (sender as Button).ContextMenu.IsEnabled = true;
            (sender as Button).ContextMenu.PlacementTarget = (sender as Button);
            (sender as Button).ContextMenu.Placement = System.Windows.Controls.Primitives.PlacementMode.Bottom;
            (sender as Button).ContextMenu.IsOpen = true;
        }

        private void MenuLoadSelected_Click(object sender, RoutedEventArgs e)
        {
            ModuleListWindow moduleList = new ModuleListWindow(loader);
            moduleList.Show();
        }

        private void MenuLoadAll_Click(object sender, RoutedEventArgs e)
        {
            loader.StartAllModules();
            RefreshModuleList();
        }

        private void MenuItemQuit_Click(object sender, RoutedEventArgs e)
        {
            ProgrammEnd();
        }

        private void MenuItemInfo_Click(object sender, RoutedEventArgs e)
        {
            modules[0].State = ModuleState.Error;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            ProgrammEnd();
        }

        private void ProgrammEnd()
        {
            refreshTimer.Stop();

            Environment.Exit(0);
        }

    }
}
