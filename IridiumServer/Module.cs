﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace IridiumServer
{
    public class Module
    {

        private string name;
        private string displayName;
        private bool started = false;
        private ModuleState state;
        

        public ModuleState State { get => state; set => state = value; }
        public bool Started { get => started; set => started = value; }
        public string Name { get => name; set => name = value; }
        public string DisplayName { get => displayName; set => displayName = value; }
        
        public Resources.ModuleGUI gui;

        public virtual void RunModule()
        {
            state = ModuleState.Inactive;
            gui = new Resources.ModuleGUI(displayName, State);
            ((MainWindow)Application.Current.MainWindow).GetPanel().Children.Add(gui);
            Started = true;
        }

        public void StopModule()
        {
            ((MainWindow)Application.Current.MainWindow).GetPanel().Children.Remove(gui);
            Started = false;
        }

        public void RefreshGUI()
        {
            gui.state = State;
            gui.Refresh();
        }
    }
}
