﻿using IridiumInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace IridiumServer
{
    public enum ModuleState
    {
        Active,
        Inactive,
        Working,
        Error,
    }

    [ServiceContract]
    public interface IWCFModuleService
    {
        [OperationContract]
        int RegisterClient();

        [OperationContract]
        List<string> ListAvailableModules();

        [OperationContract]
        void SendObject(ComplexData data);

        [OperationContract]
        ComplexData ReceiveObject(int requestingClient);

        [OperationContract]
        int DataAvailable(int requestingClient);

        [OperationContract]
        ModuleState GetModuleState(string module);
    }
}
