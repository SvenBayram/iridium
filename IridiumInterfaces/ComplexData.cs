﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace IridiumInterfaces
{
    [DataContract]
    public class ComplexData
    {
        //[DataMember]
        private string moduleAffiliation;

        //[DataMember]
        private int clientAffiliation;

        //[DataMember]
        private Queue<object> dataList;

        public ComplexData(string moduleAffiliation, int clientAffiliation, params object[] data)
        {
            this.moduleAffiliation = moduleAffiliation;
            this.clientAffiliation = clientAffiliation;

            foreach (object item in data)
            {
                DataList.Enqueue(item);
            }
        }

        public Queue<object> DataList { get => dataList; }
        public string ModuleAffiliation { get => moduleAffiliation; }
        public int ClientAffiliation { get => clientAffiliation; }
    }
}
