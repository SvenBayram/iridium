﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace IridiumClientTest
{
    class Connection
    {
        private bool runnning = false;
        private IPAddress clientIP;
        private NetworkStream stream;
        private TcpClient connection;
        private string ipRegexPattern = @"\b((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\.){3}(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[1-9])\b";
        
        private string ip;
        private int port;
        private string temp;
        private bool repeatInput = true;

        public Connection()
        {
            Regex rgx = new Regex(ipRegexPattern);
            Console.WriteLine("Multi-Client");
            while (repeatInput)
            {
                Console.WriteLine("Please enter the IP-Address of the MultiServer");
                ip = Console.ReadLine();
                if (rgx.IsMatch(ip))
                {
                    repeatInput = false;
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("Invalid input. Pls try again.");
                }
            }

            clientIP = IPAddress.Parse(ip);
            connection = new TcpClient(ip, 4711);
            stream = connection.GetStream();

            port = Convert.ToInt32(ReceiveString());
            CloseConnection();

            connection = new TcpClient(ip, port);
            stream = connection.GetStream();

            runnning = true;

            while (runnning)
            {
                Thread.Sleep(100);

                Console.WriteLine(ReceiveString());

                temp = Console.ReadLine();

                SendString(temp);

                if (temp == "stop")
                {
                    Close();
                }

            }
            

            Close();
        }

        public void SendString(string output)
        {
            Byte[] buffer = new Byte[1024];
            stream = connection.GetStream();
            buffer = Encoding.ASCII.GetBytes(output);
            stream.Write(buffer, 0, buffer.Length);
        }

        public string ReceiveString()
        {
            int numberOfBytesRead = 0;
            Byte[] buffer = new Byte[1024];
            StringBuilder receivedString = new StringBuilder();
            if (stream.CanRead)
            {
                do
                {
                    numberOfBytesRead = stream.Read(buffer, 0, buffer.Length);
                    receivedString.AppendFormat("{0}", Encoding.ASCII.GetString(buffer, 0, numberOfBytesRead));
                } while (stream.DataAvailable);
            }
            else
            {
                Console.WriteLine("Sorry. You cannot read from this NetworkStream");
            }
            return receivedString.ToString();
        }

        private void CloseConnection()
        {
            stream.Close();
            connection.Close();
            stream = null;
            connection = null;
        } 
        
        private void Close()
        {
            CloseConnection();
            Environment.Exit(0);
        }
    }
}
